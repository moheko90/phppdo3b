-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2017 at 03:19 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pegawai`
--

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `ID` int(20) NOT NULL,
  `NIK` varchar(20) NOT NULL,
  `Nama_Lengkap` varchar(100) NOT NULL,
  `Gelar_Depan` varchar(20) NOT NULL,
  `Gelar_Belakang` varchar(20) NOT NULL,
  `Identitas (KTP,SIM)` varchar(15) DEFAULT NULL,
  `Nomor_Identitas` int(20) DEFAULT NULL,
  `Tempat_Lahir` varchar(15) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Jenis_Kelamin (L,P)` varchar(5) NOT NULL,
  `Telepon` int(15) NOT NULL,
  `Hp` int(15) NOT NULL,
  `Email` int(30) NOT NULL,
  `Website` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`ID`, `NIK`, `Nama_Lengkap`, `Gelar_Depan`, `Gelar_Belakang`, `Identitas (KTP,SIM)`, `Nomor_Identitas`, `Tempat_Lahir`, `Tanggal_Lahir`, `Jenis_Kelamin (L,P)`, `Telepon`, `Hp`, `Email`, `Website`) VALUES
(145899, '998765', 'Fanizar Miftahul A', 'Drs', 'Amd', 'KTP', 9987654, 'Nabire', '1998-05-12', 'L', 678543, 987435780, 9987544, 'www.fani.com'),
(99876785, '76854347', 'Mitha Aulia N', 'Drs', 'Amd', 'KTP', 8765906, 'Klaten', '1998-09-11', 'P', 554878, 729766404, 123456789, 'www.mitha.blogspot.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
