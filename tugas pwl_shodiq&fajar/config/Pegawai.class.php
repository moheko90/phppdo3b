<?php

include_once "database.php";

class Pegawai{
	private $conn;
	public function __construct()
	{
		$data = new database();
		$db = $data->koneksi();
		$this->conn = $db;
	}
	
	public function lihat()
	{
		$sql = $this->conn->prepare("SELECT * FROM pegawai");
		return $sql;
	}
	
	public function tambah($nik, $nama_lengkap)
	{
		try {
			$sql = $this->conn->prepare("
				INSERT INTO pegawai (nik, nama_lengkap, gelar_depan,
				gelar_belakang, identitas, nomor_identitas, tempat_lahir, 
				tanggal_lahir, jenis_kelamin, telepon, hp, email, website);
				) VALUES (:nik, :nama_lengkap, :gelar_depan, :gelar_belakang,
				:identitas, :nomor_identitas, :tempat_lahir, :date,
				:jenis_kelamin, :telepon, :hp, :email, :website)
			");
			$sql->bindParam(':nik', $nik);
			$sql->bindParam(':nama_lengkap', $nama_lengkap);
			$sql->bindParam(':gelar_belakang', $gelar_belakang);
			$sql->bindParam(':identitas', $identitas);
			$sql->bindParam(':nomor_identitas', $nomor_identitas);
			$sql->bindParam(':tempat_lahir', $tempat_lahir);
			$sql->bindParam(':date', $sqlDate);
			$sql->bindParam(':jenis_kelamin', $jenis_kelamin);
			$sql->bindParam(':telepon', $telepon);
			$sql->bindParam(':hp', $hp);
			$sql->bindParam(':email', $email);
			$sql->bindParam(':website', $website);
			$sql->execute();
			return $sql;
		} catch(PDOException $e) {
			echo "Gagal". $e->getMessage();
		}
	}
}

?>