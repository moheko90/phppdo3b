-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2017 at 03:38 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pegawai`
--

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `ID` int(10) NOT NULL,
  `NIK` varchar(20) NOT NULL,
  `Nama_Lengkap` varchar(50) NOT NULL,
  `Gelar_Depan` varchar(10) NOT NULL,
  `Gelar_Belakang` varchar(10) NOT NULL,
  `Identitas (KTP,SIM)` varchar(10) DEFAULT NULL,
  `Nomor_Identitas` int(25) DEFAULT NULL,
  `Tempat_Lahir` varchar(20) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Jenis_Kelamin (L,P)` varchar(5) NOT NULL,
  `Telepon` int(15) NOT NULL,
  `Hp` int(15) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Website` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`ID`, `NIK`, `Nama_Lengkap`, `Gelar_Depan`, `Gelar_Belakang`, `Identitas (KTP,SIM)`, `Nomor_Identitas`, `Tempat_Lahir`, `Tanggal_Lahir`, `Jenis_Kelamin (L,P)`, `Telepon`, `Hp`, `Email`, `Website`) VALUES
(6128375, '81231272515', 'Anjas Setiawan', 'Ir', 'Amd', 'KTP', 517523152, 'Sukoharjo', '1998-11-10', 'L', 271436, 85746, 'anjas@gmail.com', 'anjas.blogspot.com'),
(8121231, '81628124', 'Christian Daud Widiyanto', 'Drs', 'Amd', 'KTP', 81263213, 'Boyolali', '1998-09-16', 'L', 2716171, 8581253, 'daud7058@gmail.com', 'daud.blogspot.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
