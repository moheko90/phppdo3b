<?php
	include "Config/database.php";
	include "Config/Pegawai.class.php";
?>
<a href="Pegawai_add.php">Tambah</a>
<table border="1">
	<tr>
		<th>No</th>
		<th>NIK</th>
		<th>Nama Lengkap</th>
		<th>Gelar Depan</th>
		<th>Gelar Belakang</th>
		<th>identitas</th>
		<th>Nomor Identitas</th>
		<th>Tempat Lahir</th>
		<th>Tanggal Lahir</th>
		<th>Jenis Kelamin</th>
		<th>Telepon</th>
		<th>Hp</th>
		<th>Email</th>
		<th>website</th>
		<th>Aksi</th>
	</tr>
	<?php
		$data = new Pegawai();
		$hasil = $data->lihat();
		$hasil->execute();
		$result = $hasil->fetchAll();
		$no = 1;
	?>
	<?php foreach($result as $row):?>
		<tr>
			<td><?php echo $no++;?></td>
			<td><?php echo $row['nik'];?></td>
			<td><?php echo $row['nama_lengkap'];?></td>
			<td><?php echo $row['gelar_depan'];?></td>
			<td><?php echo $row['gelar_belakang'];?></td>
			<td><?php echo $row['identitas'];?></td>
			<td><?php echo $row['nomor_identitas'];?></td>
			<td><?php echo $row['tempat_lahir'];?></td>
			<td><?php echo $row['tanggal_lahir'];?></td>
			<td><?php echo $row['jenis_kelamin'];?></td>
			<td><?php echo $row['telepon'];?></td>
			<td><?php echo $row['hp'];?></td>
			<td><?php echo $row['email'];?></td>
			<td><?php echo $row['website'];?></td>
			<td>Edit | Hapus</td>
		</tr>
	<?php endforeach;?>
</table>
