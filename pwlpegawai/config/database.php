<?php

class database{
    
 private $servername = "localhost",
  $username = "root",
  $password = "",
  $dbname = "db_pegawai";
  
  public function koneksi()
  {
    $this->conn=null;
    try {
        // buat koneksi dengan database
        $this->conn = new PDO("mysql:host=".$this->servername.";dbname=".$this->dbname,$this->username,$this->password);
        $this->conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
      }
      catch (PDOException $e) {
        // tampilkan pesan kesalahan jika koneksi gagal
        echo "Koneksi atau query bermasalah: " .$e->getMessage();
        
      }
      return $this->conn;
    }
  }
?>
  